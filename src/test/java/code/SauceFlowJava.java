package code;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.Elements;
import pages.Start;
import pages.Wd;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class SauceFlowJava {

    Elements elementsObject = new Elements();
    Start startPage = new Start(driver);
    JavascriptExecutor js = (JavascriptExecutor) driver;

    static WebDriver driver = Wd.setupDriver();
    WebDriverWait wait1 = new WebDriverWait(driver, 15);

    static DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
    Date date = new Date();
    String date1 = dateFormat.format(date);

    public SauceFlowJava() throws Throwable {
    }

    @Given("^Ingresar con usuario standard_user$")
    public void ingresar_con_usuario_standard_user(){
        driver.get(elementsObject.HOME);
        driver.manage().window().maximize();
    }

    @When("^Ingreso user y password de standard_user$")
    public void ingreso_user_y_password_de_standard_user(){
        startPage.labelUser.sendKeys("standard_user");
        startPage.labelPass.sendKeys("secret_sauce");
    }

    @Then("^Valido standard_user$")
    public void valido_standard_user(){

        startPage.clickbtnLogin();

        startPage.clickprodOne();

        startPage.clickcarShop();

        startPage.clickbtnCheckout();

        startPage.labelFirst.sendKeys("juan");

        startPage.labelLast.sendKeys("perez");

        startPage.labelPostal.sendKeys("8320000");

        startPage.clickbtnContinue();

        startPage.clickbtnFinish();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Given("^Ingresar con usuario locked_out_user$")
    public void ingresar_con_usuario_locked_out_user(){
        driver.get(elementsObject.HOME);
        //driver.manage().window().maximize();
    }

    @When("^Ingreso user y password de locked_out_user$")
    public void ingreso_user_y_password_de_locked_out_user(){
        startPage.labelUser.sendKeys("locked_out_user");
        startPage.labelPass.sendKeys("secret_sauce");
    }

    @Then("^Valido locked_out_user$")
    public void valido_locked_out_user(){

        startPage.clickbtnLogin();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Assert.assertEquals("Epic sadface: Sorry, this user has been locked out.", driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/form[1]/div[3]/h3[1]")).getText());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            Wd.takeScreenShotTest(driver, "S2-00" + " " + Wd.BROWSER
                    + " Sauce-Flow Locked User- End Case -" + date1);
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("error: " + e);
        }

    }

    @Given("^Ingresar con usuario problem_user$")
    public void ingresar_con_usuario_problem_user(){
        driver.get(elementsObject.HOME);
        //driver.manage().window().maximize();
    }

    @When("^Ingreso user y password de problem_user$")
    public void ingreso_user_y_password_de_problem_user(){
        startPage.labelUser.sendKeys("problem_user");
        startPage.labelPass.sendKeys("secret_sauce");
    }

    @Then("^Valido problem_user$")
    public void valido_problem_user(){

        startPage.clickbtnLogin();

        //  "/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]"
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Assert.assertEquals("carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.", driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]")).getText());

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        try {
            Wd.takeScreenShotTest(driver, "S3-00" + " " + Wd.BROWSER
                    + " Sauce-Flow Problem User- End Case -" + date1);
            Thread.sleep(1000);
        } catch (Exception e) {
            System.out.println("error: " + e);
        }

    }

    @Given("^Ingresar con usuario performance_glitch_user$")
    public void ingresar_con_usuario_performance_glitch_user(){
        driver.get(elementsObject.HOME);
        //driver.manage().window().maximize();
    }

    @When("^Ingreso user y password de performance_glitch_user$")
    public void ingreso_user_y_password_de_performance_glitch_user(){
        startPage.labelUser.sendKeys("performance_glitch_user");
        startPage.labelPass.sendKeys("secret_sauce");
    }

    @Then("^Valido performance_glitch_user$")
    public void valido_performance_glitch_user(){

        startPage.clickbtnLogin();

        startPage.clickprodOne();

        startPage.clickcarShop();

        startPage.clickbtnCheckout();

        startPage.labelFirst.sendKeys("juan");

        startPage.labelLast.sendKeys("perez");

        startPage.labelPostal.sendKeys("8320000");

        startPage.clickbtnContinue();

        startPage.clickbtnFinish();

        startPage.clickbtnbackHome();

        driver.close();
        driver.quit();

    }

}
