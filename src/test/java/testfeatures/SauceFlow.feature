@CucumberSauceFlow
Feature: SauceFlow

  Scenario: Validar flujo por tipo de usuario standard_user
    Given Ingresar con usuario standard_user
    When  Ingreso user y password de standard_user
    Then  Valido standard_user

  Scenario: Validar flujo por tipo de usuario locked_out_user
    Given Ingresar con usuario locked_out_user
    When  Ingreso user y password de locked_out_user
    Then  Valido locked_out_user

  Scenario: Validar flujo por tipo de usuario problem_user
    Given Ingresar con usuario problem_user
    When  Ingreso user y password de problem_user
    Then  Valido problem_user

  Scenario: Validar flujo por tipo de usuario performance_glitch_user
    Given Ingresar con usuario performance_glitch_user
    When  Ingreso user y password de performance_glitch_user
    Then  Valido performance_glitch_user

